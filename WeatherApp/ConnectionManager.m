//
//  ConnectionManager.m
//  WeatherApp
//
//  Created by Antti on 2/4/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import "ConnectionManager.h"
#import "WeatherItem.h"
#import <AFNetworking.h>

static NSString * const BaseURLString = @"https://www.raywenderlich.com/demos/weather_sample/";
// Set this to your World Weather Online API Key
static NSString * const WorldWeatherOnlineAPIKey = @"a10811918c2d497b862190155171703";
static NSString * const WorldWeatherOnlineURLString = @"http://api.worldweatheronline.com/premium/v1/";

@implementation ConnectionManager

+ (instancetype)sharedManager
{
    static ConnectionManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [ConnectionManager new];
    });
    
    return sharedManager;
}

- (OMPromise *)loadJSON
{
    OMDeferred *deferred = [OMDeferred new];
    
    NSString *string = [NSString stringWithFormat:@"%@weather.php?format=json", BaseURLString];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:string parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSArray *weather = [self parseWeatherItem:responseObject];
        [deferred fulfil:weather];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [deferred fail:error];
    }];
    
    return deferred.promise;
}

- (OMPromise *)loadPlist
{
    OMDeferred *deferred = [OMDeferred new];
    
    NSString *string = [NSString stringWithFormat:@"%@weather.php?format=plist", BaseURLString];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFPropertyListResponseSerializer serializer];
    
    [manager GET:string parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        [deferred fulfil:responseObject];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [deferred fail:error];
    }];
    
    return deferred.promise;
}

- (OMPromise *)loadXML
{
    OMDeferred *deferred = [OMDeferred new];
    
    NSString *string = [NSString stringWithFormat:@"%@weather.php?format=xml", BaseURLString];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFXMLParserResponseSerializer serializer];
    
    [manager GET:string parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSXMLParser *XMLParser = (NSXMLParser *)responseObject;
        [XMLParser setShouldProcessNamespaces:YES];
        XMLParser.delegate = self;
        
        [deferred fulfil:responseObject];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [deferred fail:error];
    }];
    
    return deferred.promise;
}


#pragma mark - Private methods

- (NSArray *)parseWeatherItem:(id)responseObject
{
    NSMutableArray *items = [NSMutableArray array];
    NSDictionary *data = responseObject[@"data"];
    NSArray *weather = data[@"weather"];
    NSArray *condition = data[@"current_condition"];
    NSDictionary *currentCondition = condition.firstObject;
    
    for (NSDictionary *currentWeather in weather) {
        WeatherItem *item = [WeatherItem new];
        
        item.date = currentWeather[@"date"];
        NSArray *list = currentWeather[@"weatherIconUrl"];
        NSDictionary *imageURL = list.firstObject;
        item.weatherIconURL = [NSURL URLWithString:imageURL[@"value"]];
        list = currentWeather[@"weatherDesc"];
        NSDictionary *desc = list.firstObject;
        item.weatherDescription = desc[@"value"];
        
        item.cloudCover = currentCondition[@"cloudcover"];
        item.humidity = currentCondition[@"humidity"];
        item.observationTime = currentCondition[@"observation_time"];
        item.pressue = currentCondition[@"pressure"];
        item.tempC = currentCondition[@"temp_C"];
        item.visibility = currentCondition[@"visibility"];
        item.windSpeedKmph = currentCondition[@"windspeedKmph"];
        
        [items addObject:item];
    }
    
    return items;
}


#pragma mark - NSXMLParser delegate methods

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    self.xmlWeather = [NSMutableDictionary dictionary];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    self.elementName = qName;
    
    if([qName isEqualToString:@"current_condition"] ||
       [qName isEqualToString:@"weather"] ||
       [qName isEqualToString:@"request"]) {
        self.currentDictionary = [NSMutableDictionary dictionary];
    }
    
    self.outstring = [NSMutableString string];
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (!self.elementName)
        return;
    
    [self.outstring appendFormat:@"%@", string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if ([qName isEqualToString:@"current_condition"] ||
        [qName isEqualToString:@"request"]) {
        self.xmlWeather[qName] = @[self.currentDictionary];
        self.currentDictionary = nil;
    }
    
    else if ([qName isEqualToString:@"weather"]) {
        
        // Initialize the list of weather items if it doesn't exist
        NSMutableArray *array = self.xmlWeather[@"weather"] ?: [NSMutableArray array];
        
        // Add the current weather object
        [array addObject:self.currentDictionary];
        
        // Set the new array to the "weather" key on xmlWeather dictionary
        self.xmlWeather[@"weather"] = array;
        
        self.currentDictionary = nil;
    }
    
    else if ([qName isEqualToString:@"value"]) {
        // Ignore value tags, they only appear in the two conditions below
    }

    else if ([qName isEqualToString:@"weatherDesc"] ||
             [qName isEqualToString:@"weatherIconUrl"]) {
        NSDictionary *dictionary = @{@"value": self.outstring};
        NSArray *array = @[dictionary];
        self.currentDictionary[qName] = array;
    }

    else if (qName) {
        self.currentDictionary[qName] = self.outstring;
    }
    
    self.elementName = nil;
}

- (void) parserDidEndDocument:(NSXMLParser *)parser
{
}

- (OMPromise *)updateWeatherAtLocation:(CLLocation *)location
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    OMDeferred *deferred = [OMDeferred new];
    
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:WorldWeatherOnlineURLString]];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    
    parameters[@"num_of_days"] = @(7);
    parameters[@"q"] = [NSString stringWithFormat:@"%f,%f",location.coordinate.latitude,location.coordinate.longitude];
    parameters[@"format"] = @"json";
    parameters[@"key"] = WorldWeatherOnlineAPIKey;
    
    [manager GET:@"weather.ashx" parameters:parameters progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSArray *weather = [self parseWeatherItem:responseObject];
        [deferred fulfil:weather];
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog(@"Error: %@", error);
        [deferred fail:error];
    }];
    
    return deferred.promise;
}

@end
