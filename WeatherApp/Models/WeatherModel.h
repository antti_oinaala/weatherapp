//
//  WeatherModel.h
//  WeatherApp
//
//  Created by Antti on 2/5/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "WeatherItem.h"

FOUNDATION_EXPORT NSString *const WeatherCellIdentifier;
FOUNDATION_EXPORT NSString *const WeatherCellNib;

@interface WeatherModel : NSObject
- (UITableViewCell *)tableView:(UITableView *)tableView representationAsCellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (CGFloat)tableView:(UITableView *)tableView representationAsHeightForRowAtIndexPath:(NSIndexPath *)indexPath;
- (CGFloat)tableView:(UITableView *)tableView representationAsEstimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath;

@property (strong, nonatomic) WeatherItem *weatherItem;

@end
