//
//  WeatherModel.m
//  WeatherApp
//
//  Created by Antti on 2/5/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import "WeatherModel.h"
#import "WeatherCell.h"
#import <AFNetworking.h>
#import "UIImageView+AFNetworking.h"

NSString *const WeatherCellIdentifier = @"WeatherCell";
NSString *const WeatherCellNib = @"WeatherCell";
CGFloat const WeatherCellHeight = 60.0f;

@implementation WeatherModel

- (UITableViewCell *)tableView:(UITableView *)tableView representationAsCellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:WeatherCellIdentifier forIndexPath:indexPath];
    
    [self setupCell:cell];
    
    [cell setNeedsDisplay];
    [cell layoutIfNeeded];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView representationAsHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return WeatherCellHeight;
}

- (CGFloat)tableView:(UITableView *)tableView representationAsEstimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return WeatherCellHeight;
}

- (void)setupCell:(UITableViewCell *)weatherCell
{
    if ([weatherCell isKindOfClass:WeatherCell.class]) {
        WeatherCell *cell = (WeatherCell *)weatherCell;

        [cell.iconImageView setImageWithURL:_weatherItem.weatherIconURL placeholderImage:nil];
        
        cell.titleLabel.text = _weatherItem.weatherDescription;
    }
}

@end
