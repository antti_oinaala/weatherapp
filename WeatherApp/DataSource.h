//
//  DataSource.h
//  WeatherApp
//
//  Created by Antti on 2/4/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "OMPromises.h"
#import <CoreLocation/CoreLocation.h>

@class WeatherModel;
@class DataSource;

@protocol WeatherCellSelectionDelegate <NSObject>
- (void)weatherModel:(WeatherModel *)model didSelectedAtIndexPath:(NSIndexPath *)indexPath;

@end

@protocol LocationUpdatingDelegate <NSObject>
- (void)dataSource:(DataSource *)dataSource didUpdateDataWithWeather:(id)weather;
- (void)dataSource:(DataSource *)dataSource didFailWithError:(NSError *)error;

@end

@interface DataSource : NSObject <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) id<WeatherCellSelectionDelegate> weatherCellSelectionDelegate;
@property (weak, nonatomic) id<LocationUpdatingDelegate> locationUpdatingDelegate;
@property (strong, nonatomic) CLLocationManager *locationManager;

- (OMPromise *)loadJSONItems;

@end
