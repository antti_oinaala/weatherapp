//
//  WTADetailViewController.h
//  WeatherApp
//
//  Created by Antti on 2/5/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WeatherItem.h"

@interface WTADetailViewController : UIViewController
@property(strong, nonatomic) WeatherItem *weatherItem;

@end
