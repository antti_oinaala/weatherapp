//
//  DataSource.m
//  WeatherApp
//
//  Created by Antti on 2/4/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import "DataSource.h"
#import "ConnectionManager.h"
#import "WeatherModel.h"

@interface DataSource()
@property (strong, nonatomic) NSArray *models;

@end

@implementation DataSource

- (instancetype)init
{
    if ((self = [super init])) {
        _locationManager = [CLLocationManager new];
        _locationManager.delegate = self;
    }
    
    return self;
}

- (OMPromise *)loadJSONItems
{
    ConnectionManager *cm = [ConnectionManager sharedManager];
    NSMutableArray *list = [NSMutableArray array];
    
    return [[cm loadJSON]fulfilled:^(id _Nullable result) {
        for (WeatherItem *item in result) {
            WeatherModel *model = [WeatherModel new];
            model.weatherItem = item;
            [list addObject:model];
        }
        
        self.models = [NSArray arrayWithArray:list];
    }];
}


#pragma mark - Location delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    // Last object contains the most recent location
    CLLocation *newLocation = [locations lastObject];
    
    // If the location is more than 5 minutes old, ignore it
    if([newLocation.timestamp timeIntervalSinceNow] > 300)
        return;
    
    [self.locationManager stopUpdatingLocation];
    
    ConnectionManager *cm = [ConnectionManager sharedManager];
    
    NSMutableArray *list = [NSMutableArray array];
    
    [[[cm updateWeatherAtLocation:newLocation]fulfilled:^(id _Nullable result) {
        for (WeatherItem *item in result) {
            WeatherModel *model = [WeatherModel new];
            model.weatherItem = item;
            [list addObject:model];
        }
        
        self.models = [NSArray arrayWithArray:list];
        
        if ([self.locationUpdatingDelegate respondsToSelector:@selector(dataSource:didUpdateDataWithWeather:)]) {
            [self.locationUpdatingDelegate dataSource:self didUpdateDataWithWeather:result];
        }
    }] failed:^(NSError *error) {
        if ([self.locationUpdatingDelegate respondsToSelector:@selector(dataSource:didFailWithError:)]) {
            [self.locationUpdatingDelegate dataSource:self didFailWithError:error];
        }
    }];
}


#pragma mark - Tableview datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeatherModel *model = _models[indexPath.row];
    
    UITableViewCell *cell = [model tableView:tableView representationAsCellForRowAtIndexPath:indexPath];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeatherModel *model = _models[indexPath.row];
    
    return [model tableView:tableView representationAsEstimatedHeightForRowAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    WeatherModel *model = _models[indexPath.row];
    
    return [model tableView:tableView representationAsEstimatedHeightForRowAtIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    WeatherModel *model = _models[indexPath.row];
    
    if ([self.weatherCellSelectionDelegate respondsToSelector:@selector(weatherModel:didSelectedAtIndexPath:)]) {
        [self.weatherCellSelectionDelegate weatherModel:model didSelectedAtIndexPath:indexPath];
    }
}

@end
