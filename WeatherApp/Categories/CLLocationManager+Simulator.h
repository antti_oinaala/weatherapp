//
//  CLLocationManager+Simulator.h
//  WeatherApp
//
//  Created by Antti on 5/7/17.
//  Copyright © 2017 Antti. All rights reserved.
//

//#ifndef TARGET_IPHONE_SIMULATOR

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface CLLocationManager (Simulator)

- (void)startSimulatingLocation;

@end

//#endif
