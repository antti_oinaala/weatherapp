//
//  CLLocationManager+Simulator.m
//  WeatherApp
//
//  Created by Antti on 5/7/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import "CLLocationManager+Simulator.h"


//#ifndef TARGET_IPHONE_SIMULATOR

@implementation CLLocationManager (Simulator)

- (void)startSimulatingLocation
{
    CLLocation *powellsTech = [[CLLocation alloc] initWithLatitude:45.523450 longitude:-122.678897];

    [self.delegate locationManager:self didUpdateLocations:@[powellsTech, powellsTech]];
}

@end

//#endif
