//
//  WTAViewController.m
//  WeatherApp
//
//  Created by Antti on 2/4/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import "WTAViewController.h"
#import "WTADetailViewController.h"
#import "WeatherModel.h"
#import "CLLocationManager+Simulator.h"

@interface WTAViewController ()
@property (strong, nonatomic) DataSource *dataSource;

@end

@implementation WTAViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.dataSource = [DataSource new];
    self.tableView.dataSource = _dataSource;
    self.tableView.delegate = _dataSource;
    
    self.dataSource.weatherCellSelectionDelegate = self;
    self.dataSource.locationUpdatingDelegate = self;
    
    [self.tableView registerNib:[UINib nibWithNibName:WeatherCellNib bundle:nil] forCellReuseIdentifier:WeatherCellIdentifier];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Button action handlers

- (IBAction)onLoadJSON:(id)sender
{
    [[_dataSource loadJSONItems] fulfilled:^(id _Nullable results) {
        [_tableView reloadData];
    }];
}

- (IBAction)onLoadPlist:(id)sender
{
}

- (IBAction)onLoadXML:(id)sender
{
}

- (IBAction)onApiTapped:(id)sender {
    [self.dataSource.locationManager startSimulatingLocation];
}


#pragma mark - Weather cell selection delegate method

- (void)weatherModel:(WeatherModel *)model didSelectedAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WTADetailViewController *vc = [storyBoard instantiateViewControllerWithIdentifier:@"DetailViewController"];
    
    if ([vc isKindOfClass:WTADetailViewController.class]) {
        vc.weatherItem = model.weatherItem;
    }
    
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - Location update delegate

- (void)dataSource:(DataSource *)dataSource didUpdateDataWithWeather:(id)weather
{
    [_tableView reloadData];
}

- (void)dataSource:(DataSource *)dataSource didFailWithError:(NSError *)error
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Error Retrieving Weather"
                                                        message:[NSString stringWithFormat:@"%@",error]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
}

@end
