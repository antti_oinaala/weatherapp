//
//  WeatherItem.m
//  WeatherApp
//
//  Created by Antti on 2/4/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import "WeatherItem.h"

@implementation WeatherItem

- (instancetype)init
{
    if ((self = [super init])) {
        _cloudCover = nil;
        _humidity = nil;
        _observationTime = nil;
        _pressue = nil;
        _tempC = nil;
        _visibility = nil;
        _windSpeedKmph = nil;
        _weatherDescription = nil;
        _weatherIconURL = nil;
        _date = nil;
    }
    
    return self;
}

- (void)setWeatherIconURL:(NSURL *)weatherIconURL
{
    if ([weatherIconURL.scheme isEqualToString:@"http"]) {
        NSURLComponents *components = [NSURLComponents componentsWithURL:weatherIconURL resolvingAgainstBaseURL:YES];
        components.scheme = @"https";
        weatherIconURL = [components URL];
    }
    _weatherIconURL = weatherIconURL;
}

@end
