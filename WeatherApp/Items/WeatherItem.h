//
//  WeatherItem.h
//  WeatherApp
//
//  Created by Antti on 2/4/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WeatherItem : NSObject
@property (strong, nonatomic) NSNumber *cloudCover;
@property (strong, nonatomic) NSNumber *humidity;
@property (strong, nonatomic) NSDate *observationTime;
@property (strong, nonatomic) NSNumber *pressue;
@property (strong, nonatomic) NSNumber *tempC;
@property (strong, nonatomic) NSNumber *visibility;
@property (strong, nonatomic) NSNumber *windSpeedKmph;
@property (strong, nonatomic) NSString *weatherDescription;
@property (strong, nonatomic) NSURL *weatherIconURL;
@property (strong, nonatomic) NSDate *date;

@end
