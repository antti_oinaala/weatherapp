//
//  ConnectionManager.h
//  WeatherApp
//
//  Created by Antti on 2/4/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OMPromises.h"
#import <CoreLocation/CoreLocation.h>

@interface ConnectionManager : NSObject <NSXMLParserDelegate>
+ (instancetype)sharedManager;

- (OMPromise *)loadJSON;
- (OMPromise *)loadPlist;
- (OMPromise *)loadXML;

- (OMPromise *)updateWeatherAtLocation:(CLLocation *)location;

@property(strong, nonatomic) NSMutableDictionary *currentDictionary;
@property(strong, nonatomic) NSMutableDictionary *xmlWeather;
@property(strong, nonatomic) NSString *elementName;
@property(strong, nonatomic) NSMutableString *outstring;

@end
