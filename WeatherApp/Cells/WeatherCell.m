//
//  WeatherCell.m
//  WeatherApp
//
//  Created by Antti on 2/5/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import "WeatherCell.h"

@implementation WeatherCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
