//
//  WeatherCell.h
//  WeatherApp
//
//  Created by Antti on 2/5/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
