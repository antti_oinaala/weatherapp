//
//  WTAViewController.h
//  WeatherApp
//
//  Created by Antti on 2/4/17.
//  Copyright © 2017 Antti. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataSource.h"

@interface WTAViewController : UIViewController <WeatherCellSelectionDelegate, LocationUpdatingDelegate>
- (IBAction)onLoadJSON:(id)sender;
- (IBAction)onLoadPlist:(id)sender;
- (IBAction)onLoadXML:(id)sender;
- (IBAction)onApiTapped:(id)sender;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
